"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cs = void 0;
const createStore = (reducer, initialState) => {
    let state = { ...initialState };
    const subscriptions = [];
    const getState = () => ({ ...state });
    const dispatchFn = (action) => {
        state = reducer(state, action);
        subscriptions.forEach((fn) => {
            fn();
        });
    };
    const subscribe = (fn) => {
        subscriptions.push(fn);
        const unsubscribe = () => {
            const index = subscriptions.indexOf(fn);
            subscriptions.splice(index, 1);
        };
        return unsubscribe;
    };
    return [getState, dispatchFn, subscribe];
};
exports.cs = createStore;
//# sourceMappingURL=index.js.map