import { DISPATCH, GET_STATE, SUBSCRIBE } from '../constants/store';
export declare type IState = Record<string, unknown>;
export interface IAction {
    type: string;
}
export declare type IReducer<S, A> = (state: S, action: A) => S;
export declare type ICallbackFn = () => void;
export declare type IUnsubscribeFn = () => void;
export declare type IGetState<S> = () => S;
export declare type IDispatch<A> = (action: A) => void;
export declare type ISubscribe = (fn: ICallbackFn) => IUnsubscribeFn;
export interface ICreateStore<S, A> {
    [GET_STATE]: IGetState<S>;
    [DISPATCH]: IDispatch<A>;
    [SUBSCRIBE]: ISubscribe;
}
export declare type ICreateStoreFn<S, A> = (reducer: IReducer<S, A>, initialState: S) => ICreateStore<S, A>;
//# sourceMappingURL=create-store.d.ts.map