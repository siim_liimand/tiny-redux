export declare type State = Record<string, unknown>;
export declare type Action = string;
export declare type Reducer<S, A> = (state: S, action: A) => S;
export declare type Callback = () => void;
export declare type Unsubscribe = () => void;
export declare type GetState<S> = () => S;
export declare type Dispatch<A> = (action: A) => void;
export declare type Subscribe = (fn: Callback) => Unsubscribe;
export declare type Store<S, A> = [GetState<S>, Dispatch<A>, Subscribe];
export declare type CreateStore<S, A> = (reducer: Reducer<S, A>, initialState: S) => Store<S, A>;
//# sourceMappingURL=store.d.ts.map