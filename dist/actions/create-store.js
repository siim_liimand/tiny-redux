"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStore = void 0;
const hooks_1 = require("preact/hooks");
const store_1 = require("../constants/store");
const createStore = (reducer, initialState) => {
    const [state, dispatch] = hooks_1.useReducer(reducer, initialState);
    const subscriptions = [];
    const getState = () => state;
    const dispatchFn = (action) => {
        dispatch(action);
        subscriptions.forEach((fn) => fn());
    };
    const subscribe = (fn) => {
        subscriptions.push(fn);
        console.log(subscriptions);
        // unsubscribe
        return () => {
            const index = subscriptions.indexOf(fn);
            subscriptions.splice(index, 1);
            console.log('rm', subscriptions);
        };
    };
    return {
        [store_1.GET_STATE]: getState,
        [store_1.DISPATCH]: dispatchFn,
        [store_1.SUBSCRIBE]: subscribe,
    };
};
exports.createStore = createStore;
//# sourceMappingURL=create-store.js.map