import { ICreateStore, IReducer } from '../interfaces/create-store';
export declare const createStore: <S, A>(reducer: IReducer<S, A>, initialState: S) => ICreateStore<S, A>;
//# sourceMappingURL=create-store.d.ts.map