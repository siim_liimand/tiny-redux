import { Action, Store, Dispatch, GetState, Reducer, State, Subscribe } from './interfaces/store';
declare const createStore: <S, A>(reducer: Reducer<S, A>, initialState: S) => Store<S, A>;
export { createStore as cs, Action, Store, Reducer, State, Dispatch, GetState, Subscribe };
//# sourceMappingURL=index.d.ts.map