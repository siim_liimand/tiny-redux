import { Action, Callback, Store, Dispatch, GetState, Reducer, State, Subscribe, Unsubscribe } from '@interfaces/store';

const createStore = <S, A>(reducer: Reducer<S, A>, initialState: S): Store<S, A> => {
  let state: S = { ...initialState };
  const subscriptions: Callback[] = [];
  const getState = (): S => ({ ...state });
  const dispatchFn = (action: A): void => {
    state = reducer(state, action);
    subscriptions.forEach((fn) => {
      fn();
    });
  };
  const subscribe = (fn: Callback): Unsubscribe => {
    subscriptions.push(fn);

    const unsubscribe: Unsubscribe = () => {
      const index = subscriptions.indexOf(fn);
      subscriptions.splice(index, 1);
    };

    return unsubscribe;
  };

  return [getState, dispatchFn, subscribe];
};

export { createStore as cs, Action, Store, Reducer, State, Dispatch, GetState, Subscribe };
